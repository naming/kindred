# encoding: utf-8

require 'rubygems'
require 'bundler'
begin
  Bundler.setup(:default, :development)
rescue Bundler::BundlerError => e
  $stderr.puts e.message
  $stderr.puts 'Run `bundle install` to install missing gems'
  exit e.status_code
end
require 'rake'

require 'jeweler'
Jeweler::Tasks.new do |gem|
  # gem is a Gem::Specification... see http://guides.rubygems.org/specification-reference/
  gem.name = 'kindred'
  gem.homepage = 'http://github.com/jkkummerfeld/kindred'
  gem.license = 'MIT'
  gem.summary = 'Library that adds string similarity measures.'
  gem.description = ''
  gem.email = 'me@jkk.name'
  gem.authors = ['Jonathan Kummerfeld']
  # dependencies defined in Gemfile
end
Jeweler::RubygemsDotOrgTasks.new

require 'rspec/core'
require 'rspec/core/rake_task'
RSpec::Core::RakeTask.new(:spec) do |spec|
  spec.pattern = FileList['spec/**/*_spec.rb']
end

require 'cucumber'
require 'cucumber/rake/task'
Cucumber::Rake::Task.new(:features) do |t|
  t.cucumber_opts = 'features --format pretty'
end

desc 'Code coverage detail'
task :simplecov do
  ENV['COVERAGE'] = 'true'
  Rake::Task['spec'].execute
end

require 'rubocop/rake_task'
RuboCop::RakeTask.new

task :default => :spec

require 'yard'
YARD::Rake::YardocTask.new
