Feature: Calculate similarity between strings

  Scenario Outline: Varying a single letter
    Given My first word is <first_word>
    And My second word is <second_word>
    When I compute <similarity_type> similarity
    Then I should get a score of <expected_score>

    Examples:
      | first_word | second_word | expected_score | similarity_type |
      | lift       | liftr       | 0.8            | edit            |
      | lift       | liftr       | 0.8            | lemma           |
      | lift       | liftr       | 0.75           | phonetic        |
      | lift       | lifts       | 0.8            | edit            |
      | lift       | lifts       | 1.0            | lemma           |
      | lift       | lifts       | 0.75           | phonetic        |
      | lift       | lyft        | 0.75           | edit            |
      | lift       | lyft        | 0.75           | lemma           |
      | lift       | lyft        | 1.0            | phonetic        |

  Scenario Outline: Different spelling, similar sound
    Given My first word is <first_word>
    And My second word is <second_word>
    When I compute phonetic similarity
    Then I should get a score of <expected_score>

    Examples:
      | first_word | second_word | expected_score |
      | cello      | chellow     | 0.5            |
      | cello      | sello       | 1.0            |
      | chellow    | sello       | 0.5            |
      | utopy      | youtopia    | 1.0            |
      | wrist      | whist       | 0.6666666666666667 |
      | wrist      | ryst        | 1.0            |
      | ryst       | whist       | 0.6666666666666667 |
      | phati      | fatty       | 1.0            |
      | nightly    | knightlee   | 1.0            |
      | nightly    | niteli      | 1.0            |
      | knightlee  | niteli      | 1.0            |
      | qlik       | click       | 1.0            |
      | click      | cliq        | 1.0            |
      | cliq       | qlik        | 1.0            |
      | qlik       | klic        | 1.0            |
      | klic       | clik        | 1.0            |

  Scenario Outline: Multiple word queries
    Given My first words are <first_words>
    And My second words are <second_words>
    When I compute a set of similarity scores
    Then I should get a <measure> score of <expected_score>

    Examples:
      | first_words     | second_words     | measure | expected_score |
      | buying word     | buy words        | lemma   | 1.0            |
      | buying word     | words buy        | lemma   | 1.0            |
      | buying word     | buy              | lemma   | 0.9            |
      | word            | buy words        | lemma   | 0.9            |
      | word            | the words        | lemma   | 1.0            |
      | task rabbit     | taskrabbit       | phonetic | 0.95          |
      | taskrabbit      | task rabbit      | phonetic | 0.95          |
      | ginger snap     | ginger snap      | lemma   | 1.0            |
      | ginger snap     | ginger software  | lemma   | 0.9            |
      | ginger snap     | ginger           | lemma   | 0.9            |
      | ginger snap     | snap             | lemma   | 0.4            |
      | ginger          | ginger snap      | lemma   | 0.9            |
      | ginger          | ginger software  | lemma   | 0.9            |
      | ginger software | ginger soap      | lemma   | 1.0            |
      | ginger software | ginger snap      | lemma   | 0.9            |
      | ginger software | ginger           | lemma   | 0.9            |
      | ginger software | software         | lemma   | 0.4            |

  Scenario Outline: Word overlap
    Given My first words are <first_words>
    And My second words are <second_words>
    When I compute how many words in the first set occur in the second
    Then I should get a score of <expected_score>

    Examples:
      | first_words | second_words    | expected_score |
      | buying word | buy words       | 1.0            |
      | buying word | words buy       | 0.5            |
      | buying word | buy other words | 0.5            |
      | words       | word buy        | 1.0            |
      | words buy   | buying          | 0.25           |
      | words buy book | word buy     | 0.38888888888888884 |
      | words buy book | word book    | 0.2222222222222222 |

