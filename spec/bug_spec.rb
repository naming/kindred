require 'timeout'
require File.expand_path(File.dirname(__FILE__) + '/spec_helper')

describe 'bugs' do
  it 'should handle this in reasonable time' do
    a = 'foresite'
    b = 'B SURE REINHART & ASSOCIATES, INC. BORESIDE SURFACE & ULTRASONIC ROTOR EXAMINATION'

    expect{
      Timeout.timeout(5) {
        a.kindred_string_similarity(b)
      }
    }.to_not raise_error
  end  
end
