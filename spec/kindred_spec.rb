require File.expand_path(File.dirname(__FILE__) + '/spec_helper')

describe 'metaphone' do
  it 'should handle empty srings' do
    expect(''.kindred_double_metaphone.at(0)).to eq('')
  end
  it 'should handle input of various lengths' do
    expect('ba'.kindred_double_metaphone.at(0)).to eq('P')
    expect('bababa'.kindred_double_metaphone.at(0)).to eq('PPP')
    expect('bababababa'.kindred_double_metaphone.at(0)).to eq('PPPPP')
    expect('bababababababa'.kindred_double_metaphone.at(0)).to eq('PPPPPPP')
  end
end

describe 'word-edit-similarity' do
  it 'should return 1.0 on match' do
    expect('hello'.kindred_word_edit_similarity('hello')).to eq(1.0)
  end
  it 'should handle empty strings' do
    expect(''.kindred_word_edit_similarity('')).to eq(1.0)
    expect('hello'.kindred_word_edit_similarity('')).to eq(0.0)
    expect(''.kindred_word_edit_similarity('hello')).to eq(0.0)
    expect('hello'.kindred_word_edit_similarity('', 1.0)).to eq(1.0)
  end
  it 'should correctly calculate similarity' do
    expect('hello'.kindred_word_edit_similarity('hell')).to eq(0.8)
    expect('hell'.kindred_word_edit_similarity('hello')).to eq(0.8)
    expect('hellO'.kindred_word_edit_similarity('hello')).to eq(0.8)
    expect('hello'.kindred_word_edit_similarity('hlelo')).to eq(0.8)
  end
  it 'should handle non-string arguments' do
    expect { '1'.kindred_word_edit_similarity(1) }.to raise_error(ArgumentError)
    expect { '1'.kindred_word_edit_similarity('1') }.to_not raise_error
  end
end

describe 'word-phonetic-similarity' do
  it 'should return 1.0 on phonetic match' do
    expect('lift'.kindred_word_phonetic_similarity('lyft')).to eq(1.0)
    expect('lift'.kindred_word_phonetic_similarity('lift')).to eq(1.0)
  end
  it 'should handle empty strings' do
    expect(''.kindred_word_phonetic_similarity('')).to eq(1.0)
    expect(''.kindred_word_phonetic_similarity('hello')).to eq(0.0)
    expect('hello'.kindred_word_phonetic_similarity('')).to eq(0.0)
    expect('hello'.kindred_word_phonetic_similarity('', 1.0)).to eq(1.0)
  end
  it 'should handle non-string arguments' do
    expect { '1'.kindred_word_phonetic_similarity(1) }.to raise_error(ArgumentError)
    expect { '1'.kindred_word_phonetic_similarity('1') }.to_not raise_error
  end
end

describe 'word-lemma-similarity' do
  it 'should return 1.0 on lemma match' do
    expect('lift'.kindred_word_lemma_similarity('lift')).to eq(1.0)
    expect('lifted'.kindred_word_lemma_similarity('lifts')).to eq(1.0)
  end
  it 'should handle empty strings' do
    expect(''.kindred_word_lemma_similarity('')).to eq(1.0)
    expect(''.kindred_word_lemma_similarity('hello')).to eq(0.0)
    expect('hello'.kindred_word_lemma_similarity('')).to eq(0.0)
    expect('hello'.kindred_word_lemma_similarity('', 1.0)).to eq(1.0)
  end
  it 'should handle non-string arguments' do
    expect { '1'.kindred_word_lemma_similarity(1) }.to raise_error(ArgumentError)
    expect { '1'.kindred_word_lemma_similarity('1') }.to_not raise_error
  end
end

describe 'string-similarity' do
  # TODO: Allow multi-word querying on one similarity type only
  it 'should return 1.0 on match ignoring word order' do
    all1 = { 'edit' => 1.0, 'phonetic' => 1.0, 'lemma' => 1.0 }
    expect('hello there'.kindred_string_similarity('hello there')).to eq(all1)
###    expect('there hello'.kindred_string_similarity('hello there')).to eq(all1)
  end
  it 'should handle empty strings' do
    all1 = { 'edit' => 1.0, 'phonetic' => 1.0, 'lemma' => 1.0 }
    all0 = { 'edit' => 0.0, 'phonetic' => 0.0, 'lemma' => 0.0 }
    expect(''.kindred_string_similarity('')).to eq(all1)
    expect(''.kindred_string_similarity('', 0.0)).to eq(all1)
    expect(''.kindred_string_similarity('hello')).to eq(all0)
    expect('hello'.kindred_string_similarity('')).to eq(all0)
    expect('hello'.kindred_string_similarity('', 1.0)).to eq(all1)
  end
  it 'should handle non-string arguments' do
    expect { '1'.kindred_string_similarity(1) }.to raise_error(ArgumentError)
    expect { '1'.kindred_string_similarity('1') }.to_not raise_error
  end
end

describe 'string-overlap' do
  it 'should return 1.0 when these words are a matching subset' do
    expect('hello there'.kindred_string_overlap('hello there')).to eq(1.0)
    expect('hello'.kindred_string_overlap('hello there')).to eq(1.0)
  end
  it 'should handle empty strings' do
    expect(''.kindred_string_overlap('')).to eq(1.0)
    expect(''.kindred_string_overlap('hello')).to eq(0.0)
    expect('hello'.kindred_string_overlap('')).to eq(0.0)
    expect(''.kindred_string_overlap('hello', true, 1.0)).to eq(1.0)
  end
  it 'should handle non-string arguments' do
    expect { '1'.kindred_string_overlap(1) }.to raise_error(ArgumentError)
    expect { '1'.kindred_string_overlap('1') }.to_not raise_error
  end
end

